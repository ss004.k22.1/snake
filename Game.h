#pragma once
#include "Snake.h"
#include <iostream>
#include <string>
#include <iostream>
#include <conio.h>
#include <stdlib.h> 
#include <time.h>       

#define LEFT 1
#define RIGHT 2
#define UP 3
#define DOWN 4

using namespace std;

class Game
{
private:
	int lever;
	bool GameOver;
	int Dir;
	Snake Player;
	Node* Food;
public:
	Game();
	void gotoxy(int x, int y);
	void setLever(int );
	bool getGameOver();
	void Foodnew();
	void Draw();
	void Input();
	// <summary>
	//
	// </summary>
	void Logic();
	void Play();
	void Exit();
};

