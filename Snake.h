#pragma once
#include "Node.h"
#include <windows.h>
#include <iostream>

using namespace std;

class Snake
{
private:
	Node* Head;
	Node* Tail;
	int count;
public:
	void gotoxy(int, int);
	Snake();
	Snake(int );
	bool Addxy(int, int, int, int);
	bool AddHead(Node*);
	bool Remove();
	int getX();
	int getY();
	bool initFood(int, int);
	bool Contra();
	int Point();
};

