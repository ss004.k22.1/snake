#include "Snake.h"

Snake::Snake()
{
	this->Head = this->Tail = nullptr;
	this->count = 0;
}



Snake::Snake(int x)
{
	Node *p=CreateNode(10, 15);
	this->gotoxy(p->x, p->y);
	cout << '@';
	this->Head = this->Tail = p;
	for (int i = 1; i < x; i++)
	{
		p = CreateNode(10 + i, 15);
		AddHead(p);
	}
	this->count = x;
}

void Snake::gotoxy(int x, int y)
{
	static HANDLE h = NULL;
	if (!h)
		h = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD c = { x, y };
	SetConsoleCursorPosition(h, c);
}

bool Snake::AddHead(Node *p)
{
	this->Head->Prev = p;
	p->Next = this->Head;
	this->Head = p;
	this->gotoxy(p->x, p->y);
	cout << '@';
	return 1;
}

bool Snake::Remove()
{
	Node* p = this->Tail;
	this->gotoxy(p->x, p->y);
	cout << ' ';
	this->Tail = this->Tail->Prev;
	this->Tail->Next = nullptr;
	delete p;
	return 1;
}

int Snake::getX() { return this->Head->x; }

int Snake::getY() { return this->Head->y; }

bool Snake::Addxy(int xd, int yd,int xf,int yf)
{
	bool k;
	Node* p = CreateNode(this->Head->x + xd, this->Head->y + yd);
	if (p->x==xf && p->y==yf)
	{ 
		this->count++;
		k = 1;
	}
	else
	{
		Remove();
		k = 0;
	}
	AddHead(p);
	return k;
}

bool Snake::Contra()
{
	int a[90][28] = { 0 };
	for (Node* p = this->Head; p != nullptr; p = p->Next)
	{
		a[p->x][p->y]++;
		if (a[p->x][p->y] == 2)
			return 1;
	}
	return 0;
}

int Snake::Point() { return this->count; }