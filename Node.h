#pragma once

struct Node
{
	Node* Next;
	Node* Prev;
	int x, y;
};

Node* CreateNode(int , int );
