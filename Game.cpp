#include "Game.h"

Game::Game()
{
    this->Food = nullptr;
	this->GameOver = 0;
	this->lever = 0;
    this->Dir = RIGHT;
}

void Game::Draw()
{
    system("cls");
    for (int y = 0; y < 28; y++)
    {
        for (int x = 0; x < 90; x++)
            if (x == 0 || y == 0 || x == 89 || y == 27)
                cout << '*';
            else
                cout << ' ';
        cout << endl;
    }
}

void Game::Play()
{
    this->Draw();
    this->gotoxy(35, 13);
    cout << " Start Game ";
    this->gotoxy(32, 14);
    cout << " Lever (1->5) : ";
    int temp;
    cin >> temp;
    this->setLever(temp);
    this->Draw();
    this->Player = Snake(5);
    this->Foodnew();
    while (!this->getGameOver())
    {
        this->Input();
        this->Logic();
        Sleep(100- this->lever*10);
    }
    this->Exit();
}

void Game::setLever(int x) { this->lever = x; }

void Game::gotoxy(int x, int y)
{
    static HANDLE h = NULL;
    if (!h)
        h = GetStdHandle(STD_OUTPUT_HANDLE);
    COORD c = { x, y };
    SetConsoleCursorPosition(h, c);
}

void Game::Input()
{
    if (_kbhit())
    {
        switch (_getch())
        {
        case 119:
            if (this->Dir !=DOWN)
                this->Dir = UP;
            break;
        case 115:
            if (this->Dir !=UP)
                this->Dir = DOWN;
            break;
        case 97:
            if (this->Dir != RIGHT)
                this->Dir = LEFT;
            break;
        case 100:
            if (this->Dir !=LEFT)
                this->Dir = RIGHT;
            break;
        case 27:
            this->GameOver = 1;
            break;
        default:
            break;
        }
    }
}

bool Game::getGameOver() { return this->GameOver; }

void Game::Logic()
{
    int xnext=0, ynext=0;
    switch (this->Dir)
    {
    case UP:
       ynext--;
        break;
    case DOWN:
        ynext++;
        break;
    case RIGHT:
        xnext++;
        break;
    case LEFT:
        xnext--;
        break;
    default:
        break;
    }
    if (this->Dir == UP || this->Dir == DOWN)
    {
        Sleep(int((100 - this->lever * 10.0f) * 7 / 11));
    }
    if (this->Player.Addxy(xnext, ynext, this->Food->x, this->Food->y) == 1)
        this->Foodnew();
    if (this->Player.Contra())
        this->GameOver = 1;
    if (this->Player.getX() == 0 || this->Player.getX() == 89 || this->Player.getY()==0 || this->Player.getY()==27)
        this->GameOver = 1;
}

void Game::Foodnew()
{
    srand(time(NULL));
    int xf, yf;
    do
    {
        xf = rand() % 88 + 1;
        yf = rand() % 26 + 1;
    } while (this->Player.initFood(xf, yf));
    this->Food = CreateNode(xf,yf);
    this->gotoxy(xf, yf);
    cout << '$';
}

void Game::Exit()
{
    Sleep(2000);
    this->Draw();
    this->gotoxy(35, 13);
    cout << " Game Over  ";
    this->gotoxy(34, 14);
    cout << " Point  : " << this->Player.Point();
    _getch();
    system("cls");
}